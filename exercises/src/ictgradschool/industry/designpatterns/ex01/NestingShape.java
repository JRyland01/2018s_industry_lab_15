package ictgradschool.industry.designpatterns.ex01;

import java.util.ArrayList;
import java.util.List;

public class NestingShape extends Shape {
    private List<Shape> childShapes = new ArrayList<>();;
    public NestingShape(){
        super();
    }
    public NestingShape(int x, int y){
        super(x,y);
    }
    public NestingShape (int x,int y, int deltaX, int deltaY){
        super(x,y, deltaX, deltaY);
    }
    public NestingShape (int x,int y, int deltaX, int deltaY, int width, int height){
        super(x,y, deltaX, deltaY, width, height);
    }
    public void move( int width , int height ){
        for (Shape c: childShapes) {
            c.move(fWidth, fHeight);
        }
       super.move(width, height);
    }
    public void paint(Painter painter ){
        painter.drawRect(fX,fY,fWidth, fHeight);
        painter.translate(fX,fY);
        for (Shape c: childShapes) {
            c.paint(painter);
        }
        painter.translate(-fX,-fY);
    }
    public void add(Shape child ) throws IllegalArgumentException{
        if(child.parent() != null){
            throw new IllegalArgumentException("Tried to add child with parent");
        }
        if(child.fX+ child.getWidth() >= this.fWidth) {
            throw new IllegalArgumentException("Child to big");
        }
        if(child.fY+ child.getHeight() >= this.fHeight) {
            throw new IllegalArgumentException("Child to big");
        }
        if(child.fX < 0){
            throw new IllegalArgumentException("To small");
        }
        if(child.fY < 0){
            throw new IllegalArgumentException("To small");
        }

        child._parent = this;
        childShapes.add(child);
    }
    public void remove(Shape child ){
        child._parent = null;
        childShapes.remove(child);
    }
    public Shape shapeAt( int index ) throws IndexOutOfBoundsException{
        return childShapes.get(index);
    }
    public int shapeCount(){
        return childShapes.size();
    }
    public int indexOf(Shape child ){
        return childShapes.indexOf(child);
    }
    public boolean contains(Shape child ){
        boolean contains = false;
        if (childShapes.contains(child)){
            contains = true;
            return contains;
        }
        return contains;
    }
}
