package ictgradschool.industry.designpatterns.ex02;

import ictgradschool.industry.designpatterns.ex01.NestingShape;
import ictgradschool.industry.designpatterns.ex01.Shape;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TreeModelAdaptor implements TreeModel{
    private Shape rootShape;
    private List<TreeModelListener> _treeModelListeners;

    public TreeModelAdaptor(Shape root){
        rootShape = root;
        _treeModelListeners = new ArrayList<TreeModelListener>();
    }
    @Override
    public Object getRoot() {
        return rootShape;
    }

    @Override
    public Object getChild(Object parent, int index) {
        Object result = null;

        if (parent instanceof NestingShape){
            NestingShape s = (NestingShape) parent;
            result = s.shapeAt(index);
        }
        return result;
    }

    @Override
    public int getChildCount(Object parent) {
        int result = 0;
        if(parent instanceof NestingShape){
            NestingShape s = (NestingShape) parent;
            result = s.shapeCount();
        }
        return result;
    }

    @Override
    public boolean isLeaf(Object node) {
        return !(node instanceof NestingShape);
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        int indexOfChild = -1;
        Shape c = (Shape) child;
        if(parent instanceof NestingShape){
            NestingShape s = (NestingShape) parent;
            indexOfChild = s.indexOf(c);
        }
        return indexOfChild;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        _treeModelListeners.add(l);

    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        _treeModelListeners.remove(l);
    }
}
